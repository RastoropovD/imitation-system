﻿using ImitationSystem.Command;
using ImitationSystem.Services;
using ImitationSystem.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ImitationSystem.ViewModel
{
    class MainPageViewModel : BaseViewModel
    {

        public ICommand SaveMatrixToFile { get; set; }
        public ICommand DoCalculate { get; set; }
        public ICommand SaveResultToCsv { get; set; }
        public ICommand ShowInfo { get; set; }
        public ICommand HideInfo { get; set; }


        private FileWorker _fileWorker;
        private ImitationService _imitationService;
        
        private decimal[,] _dataArray;
        private decimal[,] _resultData;
        private string _errorMsg = "";
        private int _infoMode = 0;
        private decimal[,] _bzRight;
        private decimal[,] _bzLeft;

        public decimal[,] DataArray => _dataArray;
        public decimal[,] ResultData => _resultData;
        public string ErrorMsg => _errorMsg;

        public decimal[,] BzRight => _bzRight;
        public decimal[,] BzLeft => _bzLeft;

        public string StartState { get; set; }
        public int StepCount { get; set; }
        public int InfoMode {
            get
            {
                return _infoMode;
            }
            set
            {
                _infoMode = value;
                OnPropertyChanged(nameof(InfoMode));
            }
        }

        private decimal _criticalValue;
        public decimal CriticalValue {
            get
            {
                return _criticalValue;
            }
            set
            {
                _criticalValue = value;
                OnPropertyChanged(nameof(CriticalValue));
            }
        }

        public MainPageViewModel()
        {
            SaveMatrixToFile = new RelayCommand(SaveMatrixToFileAction);
            ((RelayCommand)SaveMatrixToFile).IsEnabled = true;

            DoCalculate = new RelayCommand(CalculateAction);
            ((RelayCommand)DoCalculate).IsEnabled = true;

            SaveResultToCsv = new RelayCommand(SaveResultToCsvAction);
            ((RelayCommand)SaveResultToCsv).IsEnabled = true;

            ShowInfo = new RelayCommand((object obj) => { InfoMode = 1; });
            ((RelayCommand)ShowInfo).IsEnabled = true;

            HideInfo = new RelayCommand((object obj) => { InfoMode = 0; });
            ((RelayCommand)HideInfo).IsEnabled = true;


            _fileWorker = new FileWorker();
            _imitationService = new ImitationService();

            CriticalValue = 0.5M;

            MainPage.OnFileUploaded += LoadDataAction;
        }


        private void LoadDataAction(string content)
        {
            try
            {
                var lines = content.Split(';');
                var data = (from t in lines
                            where !string.IsNullOrWhiteSpace(t)
                            select t.Split(',')
                    into cells
                            select cells.Select(t1 => decimal.Parse(t1.Replace('.', ','))).ToList()).ToList();

                _dataArray = new decimal[data.Count, data.Max(p => p.Count)];
                
                for (var i = 0; i < data.Count; i++)
                {
                    if(i == 0) StartState = "1";
                    else StartState += ",0";
                    for (var j = 0; j < data[i].Count; j++)
                    {
                        _dataArray[i, j] = data[i][j];
                    }
                }

                var msg = _imitationService.CheckRowSum(_dataArray);
                if (!string.IsNullOrEmpty(msg))
                {
                    ShowMessage(msg);
                }
                OnPropertyChanged(nameof(DataArray));
                OnPropertyChanged(nameof(StartState));
            } catch(Exception ex)
            {
                ShowMessage("Ошибка загрузки файла. Неверный формат данных");
            }
        }


        private void SaveMatrixToFileAction(object obj)
        {
            _fileWorker.SaveToTxt(_dataArray);
        }


        private void CalculateAction(object obj)
        {
            try
            {
                var res = _imitationService.CountResult(_dataArray, StepCount, StartState, CriticalValue);
                _resultData = res.Item1;
                _bzRight = res.Item2;
                _bzLeft = res.Item3;
                ShowMessage("");
                OnPropertyChanged(nameof(ResultData));
                OnPropertyChanged(nameof(BzRight));
                OnPropertyChanged(nameof(BzLeft));
            }catch(Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }


        private void SaveResultToCsvAction(object obj)
        {
            _fileWorker.SaveToCsv(_resultData);
        }


        private void ShowMessage(string message)
        {
            _errorMsg = message;
            OnPropertyChanged("ErrorMsg");
        }

    }
}
