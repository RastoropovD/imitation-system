﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImitationSystem.Services
{
    public class ImitationService
    {

        public Tuple<decimal[,], decimal[,], decimal[,]> CountResult(decimal[,] incomeData, int stepCount, string startState, decimal criticalVal)
        {
            var startStateMatrix = startState.Split(',').Select(p => decimal.Parse(p.Replace('.', ','))).ToArray();
            var result = new decimal[stepCount + 1, incomeData.GetLength(0)];
            var substituteCounter = 0;
            var rightTmp = new List<decimal>();
            var leftTmp = new List<decimal[]>();

            if (incomeData.GetLength(1) != startStateMatrix.Length)
                throw new Exception("Неверный формат вектора начального состояния");

            for (var i = 0; i < startStateMatrix.Length; i++)
            {
                result[0, i] = startStateMatrix[i];
            }
            

            for (var i = 1; i < result.GetLength(0); i++)
            {
                var tmpRow = new decimal[incomeData.GetLength(0)];

                for (var j = 0; j < incomeData.GetLength(0); j++)
                {
                    for(var k = 0; k < startStateMatrix.Length; k++)
                    {
                        var t1 = incomeData[k, j];
                        var t2 = result[i - 1, k];
                        tmpRow[j] += incomeData[k, j] * result[i - 1, k];
                    }
                    tmpRow[j] = Math.Round(tmpRow[j], 3, MidpointRounding.AwayFromZero);
                }

                if(tmpRow.Max() >= criticalVal)
                {
                    
                    if(substituteCounter < 3)
                    {
                        substituteCounter++;
                        var wrongState = Array.IndexOf(tmpRow, tmpRow.Max());
                        rightTmp.Add(i+1);
                        var tmp = new decimal[incomeData.GetLength(0)];
                        Array.Copy(tmpRow, tmp, tmp.Length);
                        leftTmp.Add(tmp);
                        for (var t = 0; t < tmpRow.GetLength(0); t++)
                        {
                            tmpRow[t] = incomeData[wrongState, t];
                        }
                    } else
                    {
                        substituteCounter = 0;
                    }
                }

                for(var c = 0; c < tmpRow.GetLength(0); c++)
                {
                    result[i, c] = tmpRow[c];
                }

            }
                  

            //for(var i = 0; i < result.GetLength(0); i++)
            //{
            //    var tmpArr = new decimal[result.GetLength(1)];
            //    for(var j = 0; j < result.GetLength(1); j++)
            //    {
            //        if (result[i, j] >= criticalVal)
            //        {
            //            tmpArr[j] = j+1;
            //        }
            //    }
            //    if(tmpArr.Any(p => p > 0))
            //        leftTmp.Add(tmpArr);
            //}

            var right = new decimal[rightTmp.Count, 1];

            for(var i = 0; i < rightTmp.Count; i++)
            {
                right[i, 0] = rightTmp[i];
            }

            var left = new decimal[leftTmp.Count, result.GetLength(1)];

            for(var i = 0; i < leftTmp.Count; i++)
            {
                for(var j = 0; j < leftTmp[i].Length; j++)
                {
                    left[i, j] = leftTmp[i][j];
                }
            }

            return Tuple.Create(result, right, left);
        }



        public string CheckRowSum(decimal[,] data)
        {

            var sums = new decimal[data.GetLength(0)];

            for(var i = 0; i < data.GetLength(0); i++)
            {
                for(var j = 0; j < data.GetLength(1); j++)
                {
                    sums[i] += data[i, j];
                }
            }
            
            for(var i = 0; i < sums.Length; i++)
            {
                if(sums[i] > 1)
                {
                    return $"Wrong data in row: {i+1}";
                }
            }

            return null;
        }
    }
}
