﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImitationSystem.Services
{
    public class FileWorker
    {

        public void SaveToCsv(decimal[,] data)
        {
            if (data.Length == 0) return;

            SaveToFile($"result-{DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss")}.csv", data);
        }

        public void SaveToTxt(decimal[,] data)
        {
            if (data.Length == 0) return;

            SaveToFile($"updated-data-{DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss")}.txt", data, ";", ",");
        }

        private void SaveToFile(string fileName, decimal[,] data, string eor = "", string separator = ";")
        {
            using (var writer = new StreamWriter(fileName))
            {
                var endChar = ",";
                for (var i = 0; i < data.GetLength(0); i++)
                {
                    var content = new StringBuilder();

                    for (var j = 0; j < data.GetLength(1); j++)
                    {
                        endChar = j == data.GetLength(1) - 1 ? eor : separator;
                        content.Append($"{data[i, j].ToString("0.00", System.Globalization.CultureInfo.GetCultureInfo("en-US"))}{endChar}");
                    }
                    writer.WriteLine(content.ToString());
                }
            }
        }
    }
}
