﻿using ImitationSystem.ViewModel;
using MahApps.Metro.Controls;

namespace ImitationSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            MainPageView.DataContext = new MainPageViewModel();
        }
    }
}
